core = 7.x
api = 2

; uw_ct_bibliography_esg
projects[uw_ct_bibliography_esg][type] = "module"
projects[uw_ct_bibliography_esg][download][type] = "git"
projects[uw_ct_bibliography_esg][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_bibliography_esg.git"
projects[uw_ct_bibliography_esg][download][tag] = "7.x-1.1"
projects[uw_ct_bibliography_esg][subdir] = ""
